# Démineur

Travaux effectués par :

[![Abdoulahi Fofana](https://img.shields.io/badge/Abdoulahi%20Fofana-000000?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/users/Abdwu) 
[![Ibrahima Barry](https://img.shields.io/badge/Ibrahima%20Barry-000000?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/users/ibarry25)

Ceci est une implémentation du jeu du démineur en JAVA.

![Logo Java](https://img.shields.io/badge/Java-000000?style=for-the-badge&logo=java&logoColor=white)
![HTML5](https://img.shields.io/badge/HTML5-000000?style=for-the-badge&logo=html5&logoColor=white)

## Installation / Configuration

Prérequis :
- Javafx 
- Java  

Ensuite, il vous suffit de lancer le projet depuis :

| script.sh        | VScode                       |
| ---------------- | ---------------------------- |
| lancer le script | appuyer sur le bouton VSCode |

Remarque : N'hésitez pas à modifier le chemin du path pour Javafx si nécessaire.

## Gameplay 

Une fois le jeu lancé, il ne vous reste plus qu'à jouer ! 🎮

