import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Plateau{
    private int nbLignes;
    private int nbColonnes;
    private int pourcentageDeBombes;
    private int nbBombes;
    private List<CaseIntelligente> lesCases;

    // constructeur
    public Plateau(int nbLignes, int nbColonnes, int pourcentageDeBombes){
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.pourcentageDeBombes = pourcentageDeBombes;
        this.nbBombes = (nbLignes * nbColonnes * pourcentageDeBombes) / 100;
        this.lesCases = new ArrayList<>();
    }


    // fonction qui permet de créer toutes les cases du plateau en mettant à false les 3 attributs

    /*
     * @return void
     */
    protected void creerLesCasesVides(){
        for(int i = 0; i < this.nbLignes; i++){
            for(int j = 0; j < this.nbColonnes; j++){
                this.lesCases.add(new CaseIntelligente());
            }
        }
    }

    // fonction  permet de "donner" à chaque case toutes ses cases voisines.
    /*
     * @return void
     */
    protected void rendLesCasesIntelligentes(){
        for(int i = 0; i < this.nbLignes; i++){
            for(int j = 0; j < this.nbColonnes; j++){
                CaseIntelligente laCase = this.lesCases.get(i * this.nbColonnes + j);
                if(i > 0){
                    laCase.ajouteVoisine(this.lesCases.get((i - 1) * this.nbColonnes + j)); // case du dessus
                    if (j > 0){
                        laCase.ajouteVoisine(this.lesCases.get((i - 1) * this.nbColonnes + j - 1)); // case du dessus à gauche
                    }
                    if (j < this.nbColonnes - 1){
                        laCase.ajouteVoisine(this.lesCases.get((i - 1) * this.nbColonnes + j + 1)); // case du dessus à droite
                    }
                }
                if(i < this.nbLignes - 1){
                    laCase.ajouteVoisine(this.lesCases.get((i + 1) * this.nbColonnes + j)); // case du dessous
                    if (j > 0){
                        laCase.ajouteVoisine(this.lesCases.get((i + 1) * this.nbColonnes + j - 1)); // case du dessous à gauche
                    }
                    if (j < this.nbColonnes - 1){
                        laCase.ajouteVoisine(this.lesCases.get((i + 1) * this.nbColonnes + j + 1)); // case du dessous à droite
                    }
                }
                if(j > 0){
                    laCase.ajouteVoisine(this.lesCases.get(i * this.nbColonnes + j - 1));
                }
                if(j < this.nbColonnes - 1){
                    laCase.ajouteVoisine(this.lesCases.get(i * this.nbColonnes + j + 1));
                }
            }
        }
    }

    // fonction qui permet de poser des bombes aléatoirement sur le plateau
    /*
     * @return void
     */
    protected void poseDesBombesAleatoirement(){
        Random random = new Random();
        int i = 0;
        while(i < this.nbBombes){
            int index = random.nextInt(this.nbLignes * this.nbColonnes);
            CaseIntelligente laCase = this.lesCases.get(index);
            if(!laCase.contientUneBombe()){
                laCase.poseBombe();
                i++;
            }
        }
    }

    public int getNbLignes(){
        return this.nbLignes;
    }

    public int getNbColonnes(){
        return this.nbColonnes;
    }

    public int getNbTotalBombes(){
        return this.nbBombes;
    }


    public CaseIntelligente getCase(int numLigne, int numColonne){
        return lesCases.get(numLigne * this.nbColonnes + numColonne);
    }

    /**
     * Retourne le nombre de cases marquées
     * @return int le nombre de cases marquées
     */
    public int getNbCasesMarquees(){
        int nbCasesMarquees = 0;
        for(CaseIntelligente laCase : this.lesCases){
            if(laCase.estMarquee()){
                nbCasesMarquees++;
            }
        }
        return nbCasesMarquees;
    }

    public int nbCasesRevelee(){
        int nbCasesRevelees = 0;
        for(CaseIntelligente laCase : this.lesCases){
            if(laCase.estDecouverte()){
                nbCasesRevelees++;
            }
        }
        return nbCasesRevelees;
    }


    public void poseBombe(int x, int y){
        this.lesCases.get(x * this.nbColonnes + y).poseBombe();
    }

    public void reset(){
        for(CaseIntelligente c : this.lesCases){
            c.reset();
        }

    }

    /**
     * Affiche le plateau
     */
    public void affiche(){
        for(int i = 0; i < this.nbLignes; i++){
            for(int j = 0; j < this.nbColonnes; j++){
                Case laCase = this.lesCases.get(i * this.nbColonnes + j);
                System.out.print(laCase.toString());
            }
        }

    }

    public void nouvellePartie(){
        this.reset();
        this.creerLesCasesVides();
        this.rendLesCasesIntelligentes();
        this.poseDesBombesAleatoirement();
    }



}