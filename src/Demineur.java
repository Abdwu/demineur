import java.util.Scanner;

// Class implémentant le jeu du démineur
public class Demineur extends Plateau{
    private boolean gameOver = false;
    private int Score = 0;

    // Constructeur du Demineur
    /**
     * Constructeur du Demineur
     * @param nbLignes
     * @param nbColonnes
     * @param pourcentageDeBombes
     */
    public Demineur(int nbLignes, int nbColonnes, int pourcentageDeBombes){
        super(nbLignes, nbColonnes, pourcentageDeBombes);
    }

    /**
     * Méthode qui retourne le score du joueur
     * @return int le Score
     */
    public int getScore(){
        return this.Score;
    }

    /**
     * Méthode qui permet de réveler une Case (et ces alentours ci-besoin)
     * @param int x nb ligne 
     * @param int y nb colonne
     */
    public void reveler(int x,int y){
        if (this.getCase(x, y).contientUneBombe() ){
            this.getCase(x, y).reveler();
            this.gameOver = true;
        }
        else{

            if (String.valueOf(this.getCase(x, y).nombreBombeVoisine()).equals("0")){
                this.getCase(x, y).reveler(); // revele la case 
                this.Score++;
                //Permet de reveler ces voisin
                for (Case unCase : this.getCase(x, y).getVoisinCases()) {
                    unCase.reveler();
                    this.Score++;
                }
                // revele les voisin de voisin s'il n'y pas de bombe autour
                int bool = this.getNbLignes()-1;
                while (bool >= 0){
                for (int i = getNbLignes()-1; i >-1 ; i--) {
                    for (int j = getNbColonnes()-1; j >-1 ; j--) {
                        if (String.valueOf(this.getCase(i, j).nombreBombeVoisine()).equals("0") && this.getCase(i, j).estDecouverte()){
                            for (Case uneCase : this.getCase(i, j).getVoisinCases()) {
                                if (uneCase.reveler()){

                                    this.Score++;

                                }    

                            }
                        }
                    }


                
                }
                bool--;
            }


            
            }else{
                this.getCase(x, y).reveler();
                this.Score++;
                    
            }
    }
    }   

    /**
     * Méthode qui permet de marquer une case
     * @param x nb ligne
     * @param y nb colonne
     */
    public void marquer(int x,int y){
        if (this.getCase(x, y).estMarquee()){
            System.out.println("Case déjà marquée");
        }
        else{
            if (this.getCase(x,y).contientUneBombe()){
                this.getCase(x, y).marquer();
                this.Score++;
            }else{
                this.getCase(x, y).marquer();
                this.Score--;}
        }
    }

    /**
     * Méthode qui permet de savoir si la partie est gagnée
     * @return boolen true si gagné
     */
    public boolean estGagnee(){
        return this.nbCasesRevelee() == super.getNbLignes()*super.getNbColonnes() - super.getNbTotalBombes();

    }

    /**
     * Méthode qui permet de savoir si la partie est perdue
     * @return boolen gameOver
     */
    public boolean estPerdue(){
        return this.gameOver;
    }

    /**
     * Méthode qui permet de reset le jeu
     */
    public void reset(){
        this.gameOver = false;
        this.Score = 0;
        super.reset();
    }

    /**
     * Méthode qui permet de d'afficher la partie
     */
    public void affiche(){
        System.out.println("JEU DU DEMINEUR");
        // affichage de la bordure supérieure
        System.out.print("  ");
        for (int j=0; j<this.getNbColonnes(); j++){
            System.out.print("  "+j+" ");
        }
        System.out.print(" \n");
        
        // affichage des numéros de ligne + cases
        System.out.print("  ┌");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┬");
        }
        System.out.println("───┐");
        
        // affichage des numéros de ligne + cases
        for (int i = 0; i<this.getNbLignes(); i++){
            System.out.print(i+" ");
            for (int j=0; j<this.getNbColonnes(); j++){
                System.out.print("│ "+this.getCase(i, j).toString() + " ");
            }
            System.out.print("│\n");
            
            if (i!=this.getNbLignes()-1){
                // ligne milieu
                System.out.print("  ├");
                for (int j=0; j<this.getNbColonnes()-1; j++){
                        System.out.print("───┼");
                }
                System.out.println("───┤");
            }
        }

        // affichage de la bordure inférieure
        System.out.print("  └");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┴");
        }
        System.out.println("───┘");
        
        // affichage des infos 
        System.out.println("Nombres de bombes à trouver : " + this.getNbTotalBombes());
        System.out.println("Nombres de cases marquées : " + this.getNbCasesMarquees());
        System.out.println("Score : " + this.getScore());
    }

    /**
     * Méthode qui permet de jouer une nouvelle partie
     * 
     */
    public void nouvellePartie(){
        this.reset();
        this.poseDesBombesAleatoirement();
        this.affiche();
        Scanner scan = new Scanner(System.in).useDelimiter("\n");

        while (!this.estPerdue() || this.estGagnee()){
            System.out.println("Entrer une instruction de la forme R 3 2 ou M 3 2\npour Révéler/Marquer la case à la ligne 3 et à la colonne 2");
            String [] s = scan.nextLine().split(" ");
            String action = s[0];
            int x = Integer.valueOf(s[1]);
            int y = Integer.valueOf(s[2]);
            if (action.equals("M") || action.equals("m"))
                this.marquer(x, y);
            else if (action.equals("R") || action.equals("r"))
                this.reveler(x, y);
            this.affiche();
        }


    }
}
