import java.util.List;
import java.util.ArrayList;



public class CaseIntelligente extends Case{

    private List<Case> lesVoisines;


    public CaseIntelligente(){
        super();
        this.lesVoisines = new ArrayList<>();
    }

    public void ajouteVoisine(Case uneCase){
        this.lesVoisines.add(uneCase);
    }
    

    public int nombreBombeVoisine(){
        int nombreBombe = 0;
        for (Case uneCase : lesVoisines) {
            if(uneCase.contientUneBombe()){
                nombreBombe++;
            }
        }
        return nombreBombe;
    }

    public List<Case> getVoisinCases(){

        return this.lesVoisines;

    }


    public boolean estMarquee() {
        return super.estMarquee();
    }

    /**
     * Méthode qui permet d'affichier le contenue de la case
     * @return String l'affichage de la case
     */
    public String toString(){
        if (!estMarquee() && !estDecouverte()){
            return " ";
        }
        if (estMarquee()){
            return "?";
        }
        if (reveler() && contientUneBombe()){
            return "@";
        }
        if (String.valueOf(nombreBombeVoisine()).equals("0")){
            for (Case uneCase : this.lesVoisines) {
                uneCase.reveler();
            }
        }
        return String.valueOf(nombreBombeVoisine());
    }

}